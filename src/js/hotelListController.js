(function () {

  'use strict';

  var module = angular.module('finder.Controllers');
  module.controller('HotelListController', function ($scope, $element, HotelService, Config, FilterModel) {

    $scope.$watchCollection(
      function () { return FilterModel.filteredItems; },
      angular.bind(this,
        function (newVal) {
          //console.log('HotelListController::$watch() - filteredItems changed');
          this.items = [];
          this.showMore();
          $element.removeClass('hidden');
          $element[0].scrollTop = 0;
        }
      )
    );

    // item display:
    this.items = [];
    this.filter = FilterModel.filter;

    // -- HELPERS --
    this.showMore = function () {
      if (!this.hasMoreResults()) return;
      var len = this.items.length;
      this.items.push.apply(
        this.items,
        FilterModel.filteredItems.slice(len, len + Config.PAGE_SIZE)
      );
    };

    this.invertSort = function() {
      this.sort(this.getCurrentSortField());
    };

    this.hasMoreResults = function () {
      return this.items.length < FilterModel.filteredItems.length;
    };

    this.getTotalResults = function() {
      return FilterModel.filteredItems.length;
    };

    this.getCurrentSortField = function() {
      return FilterModel.sortField;
    };

    this.getCurrentSortOrder = function() {
      return FilterModel.sortOrder === 1 ? 'asc' : 'desc';
    };

    this.sort = function(field) {
      FilterModel.sort(field);
      this.items.length = 0;
      this.showMore();
    };

  });

})();
