(function () {

  'use strict';

  var module = angular.module('finder.Services');
  module.service('Config', function () {

    return {
      CURRENCY_SYMBOL: '€',
      DISTANCE_UNITS: 'km',
      MAX_USER_RATING: 5,
      MAX_TRP_RATING: 5,
      MAX_STARS: 5,
      PAGE_SIZE: 10,
      DEFAULT_SORT: 'Distance',
      COST_OPTIONS: [0, 100, 500, 1000, 5000]
    };

  });

})();
