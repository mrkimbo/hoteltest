
(function() {

  'use strict';

  var module = angular.module('finder.Services');
  module.service('HotelService', function($http) {

    var ENDPOINT = 'data/hotels.json';

    return {
      getHotels: function() {
        return $http.get(ENDPOINT);
      }
    };

  });

})();
