(function () {

  'use strict';

  var module = angular.module('finder.Services');
  module.service('FilterModel', function () {

    return {

      items: [],
      filteredItems: [],
      sortField: '',
      sortOrder: 1,

      filter: {
        name: '',
        stars: 0,
        userRating: 0,
        trpRating: 0,
        minCost: 0
      },

      init: function(itemCollection) {
        this.items = itemCollection;
        this.filter.stars = 1;
        this.filter.userRating = 0;
        this.filter.minCost = 0;
        this.update();
      },

      update: function() {
        this.applyFilter();
        this.sort();
      },

      // filter results using settings
      applyFilter: function () {
        //console.log('FilterModel::ApplyFilter',this.filter);
        this.filteredItems = this.items.filter(function (item) {
          if (this.filter.name) {
            if(!new RegExp('^' + this.filter.name,'i').test(item.Name)) {
              return false;
            }
          }
          // ToDo: Add TrpRating
          return item.Stars >= this.filter.stars &&
              item.UserRating >= this.filter.userRating &&
              item.MinCost >= this.filter.minCost;
        }, this);
      },

      // Apply sorting to filtered results
      sort: function(field) {
        // set props:
        if (this.sortField === field) {
          // flip existing sort order
          this.sortOrder = 1 - this.sortOrder;
        } else {
          this.sortOrder = 1;
        }
        this.sortField = field || this.sortField;

        this.filteredItems.sort(this.getSortFn());
      },

      // generate sort function for either numeric or string values
      getSortFn: function() {
        var asc = this.sortOrder === 1, field = this.sortField;
        if(typeof this.filter[field] !== 'string') {
          return function(a, b) {
            return asc ? a[field]-b[field] : b[field]-a[field];
          };
        } else {
          return function (a, b) {
            if (asc) {
              return a[field] > b[field] ? b[field] : a[field];
            } else {
              return a[field] < b[field] ? b[field] : a[field];
            }
          };
        }
      },

      // find max range for field
      getMaxRating: function (field) {
        var cacheProp = 'max_' + field;
        if (!this[cacheProp]) {
          this[cacheProp] = this.items.reduce(function (prev, curr) {
            return Math.max(prev || 0, curr[field]);
          }) + 1;
        }
        return this[cacheProp];
      }
    };

  });


})();
