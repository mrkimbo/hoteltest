
(function() {

  'use strict';

  angular.module('finder.Directives')
    .directive('search', function() {

      return {

        restrict: 'AE',
        templateUrl: 'templates/search.html',
        replace: true,
        controller: function($scope, Config, FilterModel) {

          this.filter = FilterModel.filter;
          this.costRange = Config.COST_OPTIONS;

          // Watch filter for changes and applyFilter:
          $scope.$watchCollection(
            angular.bind(this, function(){ return this.filter; }),
            function(newVal) {
              //console.log('SearchDirective::$watch() - filter changed', newVal);
              FilterModel.update();
            }
          );

          // Watch name property for changes and sanitize:
          $scope.$watch(
            angular.bind(this, function(){ return this.filter.name; }),
            angular.bind(this, function(newVal) {
              this.filter.name = this.filter.name.replace(/[^0-9a-z]/gi,'');
            })
          );

        },
        controllerAs: 'search',
        bindToController: true,
        link: function($scope, $element) {

        }

      };

    });

})();
