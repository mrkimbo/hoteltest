(function () {

  'use strict';

  angular.module('finder.Directives')
    .directive('result', function () {

      return {

        restrict: 'AE',
        templateUrl: 'templates/result.html',
        transclude: true,
        controllerAs: 'ctrl',
        bindToController: true,
        controller: function ($scope) {

          this.getStarOpacity = function (idx) {
            return idx < $scope.item.Stars ? 1 : 0.25;
          };

        },
        link: function ($scope, $element) {

          // handle missing images - show backup
          $element.find('img').on('error', function () {
            this.src = './img/question-mark.png';
          });

        }

      };

    });

})();
