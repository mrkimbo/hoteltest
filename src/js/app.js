
(function() {

  'use strict';

  angular.module('finder.Services',[]);
  angular.module('finder.Controllers',['finder.Services']);
  angular.module('finder.Directives',['finder.Controllers']);

  angular.module('finder',[
    'ngTouch',
    'finder.Directives',
    'finder.Controllers',
    'finder.Services'
  ]);

})();

