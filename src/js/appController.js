(function () {

  'use strict';

  var module = angular.module('finder.Controllers');
  module.controller('AppController', function ($element, Config, HotelService, FilterModel) {

    // init props:
    this.currencySymbol = Config.CURRENCY_SYMBOL;
    this.distanceUnits = Config.DISTANCE_UNITS;
    this.starRange = [];
    this.userRatingRange = [];
    this.trpRatingRange = [];
    this.filterDrawerOpen = false;
    this.resultsHeight = 400;
    this.resultsElement = document.querySelector('#results');

    // set default sort order:
    FilterModel.sortField = Config.DEFAULT_SORT;
    FilterModel.sortOrder = 1; // asc

    // Show main content:
    this.init = function () {
      $element.removeClass('hidden');
      window.addEventListener('resize', angular.bind(this, this.onResize));
      window.addEventListener('orientationchange', angular.bind(this, this.onResize));
      ['webkitTransitionEnd','transitionend','oTransitionEnd'].forEach(
        angular.bind(this, function(evt) {
          this.resultsElement.addEventListener(
            evt, angular.bind(this, this.onResize)
          );
        })
      );
      this.onResize();
    };

    // ToDo: Add loader view-state for data-load
    // Load Data:
    HotelService.getHotels().then(
      angular.bind(this, function (evt) {
        FilterModel.init(evt.data.Establishments);
        this.createRanges();
      }),
      function (evt) {
        // ToDo: Add view-state for data-failure
        console.log(evt.data);
      }
    );

    // -- HELPERS --
    this.toggleFilterDrawer = function () {
      this.filterDrawerOpen = !this.filterDrawerOpen;
    };
    this.createRanges = function () {
      this.starRange = createRange('Stars');
      this.userRatingRange = createRange('UserRating');
      //console.log(this.starRange);
    };
    this.getRangeMax = function(field) {
      return this[field + 'Range'].length-2;
    };

    // calculate height for results panel based on available space
    this.onResize = function (evt) {
      //console.log('AppController::onResize()', window.innerHeight);
      //var y = parseInt(window.getComputedStyle(this.resultsElement).paddingTop);
      //this.resultsElement.style.height = (window.innerHeight - y) + 'px';
    };

    // Create range array for generating dropdowns
    function createRange(field) {
      var a = [];
      var min = field === 'UserRating' ? 0 : 1;
      for (var i = min; i < FilterModel.getMaxRating(field); i++) {
        a.push(i);
      }
      return a;
    }

    // Initialise
    this.init();

  });

})();
