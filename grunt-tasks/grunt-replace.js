module.exports = function (grunt) {

  grunt.registerMultiTask('replace', '', function () {
    var s;

    grunt.log.writeln('Applying regex replace to following files:');
    this.filesSrc.forEach(function (file) {
      grunt.log.writeln('    - '.white + file.cyan);
      s = grunt.file.read(file);
      this.data.options.forEach(function (item) {
        s = s.replace(new RegExp(item.find), item.repl);
      });
      grunt.file.write(file, s);
    }, this);
  });

};
