
module.exports = function (grunt) {

  grunt.registerMultiTask('cdnInject', '', function () {
    var s;

    grunt.log.writeln('Injecting CDN resource paths for following files:');
    this.filesSrc.forEach(function (file) {
      grunt.log.writeln('    - '.white + file.cyan);
      s = grunt.file.read(file);
      this.data.options.rules.forEach(function (item) {
        s = s.replace(new RegExp(item.local), item.cdn);
      });
      grunt.file.write(file, s);
    }, this);
  });

};
