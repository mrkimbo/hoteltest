
module.exports = function (grunt) {

  'use strict';

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);
  grunt.task.loadTasks('grunt-tasks');

  // configurable paths
  var _config = {
    src: 'src',
    deploy: 'deploy',
    test: 'test',
    tmp: '.tmp'
  };

  grunt.initConfig({

    config: _config,

    'http-server': {
      dev: {
        root: '<%= config.src %>',
        port: 9000,
        ext: 'html'
      },
      prod: {
        root: '<%= config.deploy %>',
        port: 9000,
        ext: 'html'
      },
      root: {
        root: './',
        port: 9000,
        ext: 'html'
      }
    },
    open: {
      dev: {
        path: 'http://localhost:9000/'
      }
    },
    clean: {
      dist: [
        '<%= config.deploy %>'
      ],
      tmp: [
        '<%= config.tmp %>'
      ]
    },
    less: {
      dist: {
        options: {
          paths: ['<%= config.src %>/styles/']
        },
        files: {
          '<%= config.src %>/styles/main.css': '<%= config.src %>/styles/main.less'
        }
      }
    },
    karma: {
      unit: {
        configFile: '<%= config.test %>/karma.conf.js',
        singleRun: true
      }
    },
    useminPrepare: {
      html: '<%= config.src %>/index.html',
      options: {
        dest: '<%= config.deploy %>'
      }
    },
    usemin: {
      html: ['<%= config.deploy %>/{,*/}*.html'],
      css: ['<%= config.deploy %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= config.deploy %>']
      }
    },
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/img',
          src: '{,*/}*.{png,jpg,jpeg,svg}',
          dest: '<%= config.deploy %>/img'
        }]
      }
    },
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.tmp %>/concat/js',
          src: '*.js',
          dest: '<%= config.tmp %>/concat/js'
        }]
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>',
          dest: '<%= config.deploy %>',
          src: [
            '*.html',
            'js/**/*.html',
            'styles/fonts/*',
            'data/*',
            'templates/*'
          ]
        }]
      }
    },
    replace: {
      removeAnnoyingDotThing: {
        options: [{
          // remove strange dot that keeps getting appended to files //
          find: /.$/,
          repl: ''
        }],
        src: [
          '<%= config.deploy %>/**/*.{html,json,js,css}'
        ]
      }
    },
    cdnInject: {
      dist: {
        options: {
          rules: [{
            local: 'components/angular/angular.min.js',
            cdn: '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js'
          },{
            local: 'components/angular-touch/angular-touch.min.js',
            cdn: '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-touch.min.js'
          },{
            local: 'components/es5-shim/es5-shim.min.js',
            cdn: '//cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js'
          },{
            local: 'components/json3/lib/json3.min.js',
            cdn: '//cdnjs.cloudflare.com/ajax/libs/json3/3.2.4/json3.min.js'
          }]
        },
        src: [
          '<%= config.deploy %>/*.html'
        ]
      }
    }
  });

  grunt.registerTask('serve', function (env) {
    grunt.task.run([
      'http-server:' + (env || 'root')
    ]);
  });

  grunt.registerTask('test', [
    'karma'
  ]);

  grunt.registerTask('compile', [
    'clean',
    //'jshint',
    'less',
    'useminPrepare',
    'imagemin',
    'concat',
    'copy',
    'cssmin',
    'ngmin',
    'uglify',
    'usemin',
    'replace',
    'cdnInject',
    'clean:tmp'
  ]);

  grunt.registerTask('build', [
    'install',
    'compile',
    'test'
  ]);

  grunt.registerTask('default', function () {

    grunt.log.writeln('\nTASKS:'.bold.yellow.underline);
    grunt.log.writeln('build'.bold.green + '\t\t\t- Install, build and test the project'.green);
    grunt.log.writeln('serve:(root|dev|prod)'.bold.green + '\t- Serve project on localhost:9000 (default: root)'.green);
    grunt.log.writeln('\nSubtasks:'.bold.yellow.underline);
    grunt.log.writeln('install'.bold.green + '\t\t\t- Install bower dependencies'.green);
    grunt.log.writeln('compile'.bold.green + '\t\t\t- Compile the project'.green);
    grunt.log.writeln('test'.bold.green + '\t\t\t- Run unit tests'.green);

  });
};
