// Karma UnitTest config

module.exports = function (config) {

  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // list of files to exclude
    exclude: [],

    // testing frameworks to use
    frameworks: ['jasmine'],

    // test results reporter to use
    // possible values: dots || progress || growl
    reporters: ['mocha'],

    // web server port
    port: 8080,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    plugins: [
      'karma-jasmine',
      'karma-mocha-reporter',
      'karma-phantomjs-launcher'
    ],

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 5000,

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true,

    // list of files / patterns to load in the browser
    files: [
      // 3rd Party Code
      'src/components/angular/angular.js',
      'src/components/angular-mocks/angular-mocks.js',

      // App-specific Code
      'src/js/app.js',
      'src/js/components/**/*.js',
      'src/js/*Controller.js',
      'src/js/app.js',

      // Suites and mocks
      'test/spec/**/*_{spec,mocks}.js'
    ]

  });

};
