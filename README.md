Hotel Finder App - A better Angular project.
=====================================================

##Installation:
Install grunt and bower globally:
```
npm install -g grunt-cli bower
```

####Install Dependencies
```
$ npm install
$ bower install
```

### Build Project
```
$ grunt build
```

### Serve
```
$ grunt serve:prod
$ open http://localhost:9000
```